/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eventos_Deportivos;

import Conexion.conexion;
import com.toedter.calendar.JDateChooser;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author jagelvis1
 */
public class eventos extends conexion {
    
    Connection cn;//variable conexion
    
    
    //se envian parametros 
    public void registrar_evento(JComboBox deportes,JTextField evento,JComboBox fase,JComboBox categoria,JComboBox ciclo_competitivo,String pais,String entidad,String municipio,String parroquia,JTextField instalacion,JFormattedTextField hora, JDateChooser fecha_desde,JDateChooser fecha_hasta){
        try{
            //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
            //realizar la consulta
            String sql="insert into eventos(deporte_evt,evento_evt,fase_evt,categoria_evt,ciclocompetitivo_evt,pais_evt,entidad_evt,municipio_evt,parroquia_evt,instalacion_evt,hora_evt,desde_evt,hasta_evt) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";                    
            
            //preparar la consulta
            PreparedStatement pst = cn.prepareCall(sql);                             
            
            //fijar los datos en las columnas
            
            pst.setString(1, (String) deportes.getSelectedItem());
            pst.setString(2, evento.getText());
            pst.setString(3, (String) fase.getSelectedItem());
            pst.setString(4, (String) categoria.getSelectedItem());
            pst.setString(5, (String) ciclo_competitivo.getSelectedItem());
            pst.setString(6, pais);
            pst.setString(7, entidad);
            pst.setString(8, municipio);
            pst.setString(9, parroquia);
            pst.setString(10, instalacion.getText());
            pst.setString(11, hora.getText());
            String fechaString = null;//asignar variable temporal
            java.util.Date fecha_1=fecha_desde.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fechaString=sdf.format(fecha_1);
            pst.setString(12, fechaString);//enviar dato estructura
            //a la columna correspondiente
            
            String fechaString_2 = null;//asignar variable temporal
            java.util.Date fecha_2 = fecha_hasta.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf_2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fechaString_2=sdf_2.format(fecha_2);
            pst.setString(13, fechaString_2);//enviar dato estructura
            //a la columna correspondiente
            
            //ejecutar la consulta
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Se ha registrado evento");
            
            //cerrar la conexion
            pst.close();                                                            
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
public void modificar_evento(JComboBox deportes,JTextField evento,JComboBox fase,JComboBox categoria,JComboBox ciclo_competitivo,String pais,String entidad,String municipio,String parroquia,JTextField instalacion,JFormattedTextField hora ,JDateChooser fecha_desde,JDateChooser fecha_hasta){
        try{
            
            conexion con = new conexion();
            cn=con.conectar();
            
            //sustituir en la consulta el valor como condicion de busqueda
            String sql="UPDATE eventos SET deporte_evt=?, modalidad_evt=?,evento_evt=?,fase_evt=?,ciclocompetitivo_evt=?,pais_evt=?,entidad_evt=?,municipio_evt=?,parroquia_evt=?,instalacion_evt=?,hora_evt=?,desde_evt=?,hasta_evt=? WHERE evento_evt='"+evento.getText()+"';";                    
            
           //preparar la consulta
            PreparedStatement pst = cn.prepareCall(sql);                             
            
            //fijar los datos en las columnas
            
            pst.setString(1, (String) deportes.getSelectedItem());
            pst.setString(2, evento.getText());
            pst.setString(3, (String) fase.getSelectedItem());
            pst.setString(4, (String) categoria.getSelectedItem());
            pst.setString(5, (String) ciclo_competitivo.getSelectedItem());
            pst.setString(6, pais);
            pst.setString(7, entidad);
            pst.setString(8, municipio);
            pst.setString(9, parroquia);
            pst.setString(10, instalacion.getText());
            pst.setString(11, hora.getText());
            String fechaString = null;//asignar variable temporal
            java.util.Date fecha_1=fecha_desde.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fechaString=sdf.format(fecha_1);
            pst.setString(12, fechaString);//enviar dato estructura
            //a la columna correspondiente
            
            String fechaString_2 = null;//asignar variable temporal
            java.util.Date fecha_2 = fecha_hasta.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf_2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fechaString_2=sdf_2.format(fecha_2);
            pst.setString(13, fechaString_2);//enviar dato estructura
            //a la columna correspondiente 
            
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Se ha actualizado registro de evento");
                                                                    
            pst.close();                                                            
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error: No se puede actualizar evento");
        }
    }

//Eliminar ya que no es necesario solo colocar set date para saber el año del calendario de evento solo guardar-modificar
public void borrar_evento(JTextField num_evento){
        //mensaje para confirmar accion
        int confirma=JOptionPane.showConfirmDialog(null, "¿Desea Eliminar Registro de Evento?");
        if(confirma==JOptionPane.YES_OPTION){
            
        try{
            conexion con = new conexion();
            cn=con.conectar();
            
            Statement s=cn.createStatement();
            
            int sql=s.executeUpdate("DELETE FROM eventos WHERE num_evt='"+num_evento.getText()+"';");                    
            
            if(sql==1){ 
                 JOptionPane.showMessageDialog(null, "Se ha Eliminado Registro de evento");
            }else{ 
                 JOptionPane.showMessageDialog(null, "Error: Registro de Evento No existe");
            
            }                       
                                                
            cn.close();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }else{
            
     }
    
 }
    
}
