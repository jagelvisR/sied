/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eventos_Deportivos;

import Conexion.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jagelvis2
 */
public class Consulta_Calendario {
    
     Connection cn;//variable conexion
     
     public void consulta_calendario(JTable tabla){//metodo para llenar la tabla
        //se envian parametros
        try {//intenta realizar estas intrucciones
            
            //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn = con.conectar();
            
            Statement consulta = cn.createStatement();//guardar estado de la conexion
            
            //consulta a la base de datos
            String columnas, sql; //variables para armar tabla
            
            
            //obtener todos los datos de la tabla
            sql="select deporte_evt,evento_evt,fase_evt,categoria_evt,ciclocompetitivo_evt,pais_evt,entidad_evt,municipio_evt,parroquia_evt,instalacion_evt,desde_evt,hasta_evt from eventos";
            ResultSet rstb=consulta.executeQuery(sql);
            ResultSetMetaData rsmd=rstb.getMetaData();//libreria metada data
            
            int col=rsmd.getColumnCount();//contar cuantas columnas
            
            DefaultTableModel modelo= new DefaultTableModel();
            modelo.addColumn("Deporte");
            modelo.addColumn("Evento");
            modelo.addColumn("Fase");
            modelo.addColumn("Categoria");
            modelo.addColumn("Ciclo Competitivo");
            modelo.addColumn("País");
            modelo.addColumn("Entidad");
            modelo.addColumn("Municipio");
            modelo.addColumn("Parroquia");
            modelo.addColumn("Instalación"
                    + " Deportiva");
            modelo.addColumn("Fecha Inicio");
            modelo.addColumn("Fecha Fin");
            
           
               //agregar filas
               while(rstb.next()){//leer registro
                 
                   String fila[]= new String[col];//arreglo
                   for(int j=0;j<col;j++){
                       fila[j]=rstb.getString(j+1);
                   } 
                   modelo.addRow(fila);
               }
               tabla.setModel(modelo);
               rstb.close();
               cn.close();
               
            
        //si no pasa al catch
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
     }
}
