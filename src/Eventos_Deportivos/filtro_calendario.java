/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eventos_Deportivos;

import Conexion.conexion;
import com.toedter.calendar.JDateChooser;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jagelvis1
 */
public class filtro_calendario {
    
    Connection cn;//variable conexion

    public void filtrar_calendario_deporte(JTable tabla, JComboBox deporte,JComboBox fase,JDateChooser inicio,JDateChooser fin){//metodo para llenar la tabla
        //se envian parametros
        try {//intenta realizar estas intrucciones
            
            //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
            Statement consulta = cn.createStatement();//guardar estado de la conexion
            
            //consulta a la base de datos
            String columnas, sql; //variables para armar tabla
            
            String fecha_inicio = null;//asignar variable temporal
            java.util.Date fecha=(java.util.Date) inicio.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_inicio=sdf.format(fecha);
            
            String fecha_fin=null;//asignar variable temporal
            java.util.Date fecha2=(java.util.Date) fin.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_fin =sdf2.format(fecha2);
            
            
            //obtener todos los datos de la tabla
            sql="select deporte_evt,evento_evt,fase_evt,categoria_evt,ciclocompetitivo_evt,pais_evt,entidad_evt,municipio_evt,parroquia_evt,instalacion_evt,desde_evt,hasta_evt from eventos where deporte_evt ='"+deporte.getSelectedItem()+"' and fase_evt='"+fase.getSelectedItem()+"' and desde_evt='"+fecha_inicio+"' and hasta_evt='"+fecha_fin+"'";
            ResultSet rstb=consulta.executeQuery(sql);
            ResultSetMetaData rsmd=rstb.getMetaData();//libreria metada data
            
            int col=rsmd.getColumnCount();//contar cuantas columnas
            
            DefaultTableModel modelo= new DefaultTableModel();
            modelo.addColumn("Deporte");
            modelo.addColumn("Evento");
            modelo.addColumn("Fase");
            modelo.addColumn("Categoria");
            modelo.addColumn("Ciclo Competitivo");
            modelo.addColumn("País");
            modelo.addColumn("Entidad");
            modelo.addColumn("Municipio");
            modelo.addColumn("Parroquia");
            modelo.addColumn("Instalación"
                    + " Deportiva");
            modelo.addColumn("Fecha Inicio");
            modelo.addColumn("Fecha Fin");
            
           
               //agregar filas
               while(rstb.next()){//leer registro
                 
                   String fila[]= new String[col];//arreglo
                   for(int j=0;j<col;j++){
                       fila[j]=rstb.getString(j+1);
                   } 
                   modelo.addRow(fila);
               }
               tabla.setModel(modelo);
               rstb.close();
               cn.close();
               
            
        //si no pasa al catch
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    
    }
    
    public void filtrar_calendario_entidad(JTable tabla,JComboBox pais,JComboBox entidad, JComboBox fase,JDateChooser inicio,JDateChooser fin){//metodo para llenar la tabla
        //se envian parametros
        try {//intenta realizar estas intrucciones
            
            //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
            Statement consulta = cn.createStatement();//guardar estado de la conexion
            
            //consulta a la base de datos
            String columnas, sql; //variables para armar tabla
            
            String fecha_inicio=null;//asignar variable temporal
            java.util.Date fecha=(java.util.Date) inicio.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_inicio=sdf.format(fecha);
            
            String fecha_fin=null;//asignar variable temporal
            java.util.Date fecha2=(java.util.Date) fin.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_fin =sdf2.format(fecha2);
            
            
            //obtener todos los datos de la tabla
            sql="select deporte_evt,evento_evt,fase_evt,categoria_evt,ciclocompetitivo_evt,pais_evt,entidad_evt,municipio_evt,parroquia_evt,instalacion_evt,desde_evt,hasta_evt from eventos where pais_evt ='"+pais.getSelectedItem()+"' and entidad_evt ='"+entidad.getSelectedItem()+"' and fase_evt='"+fase.getSelectedItem()+"'and desde_evt='"+fecha_inicio+"' and hasta_evt='"+fecha_fin+"'";
            ResultSet rstb=consulta.executeQuery(sql);
            ResultSetMetaData rsmd=rstb.getMetaData();//libreria metada data
            
            int col=rsmd.getColumnCount();//contar cuantas columnas
            
            DefaultTableModel modelo= new DefaultTableModel();
            modelo.addColumn("Deporte");
            modelo.addColumn("Evento");
            modelo.addColumn("Fase");
            modelo.addColumn("Categoria");
            modelo.addColumn("Ciclo Competitivo");
            modelo.addColumn("País");
            modelo.addColumn("Entidad");
            modelo.addColumn("Municipio");
            modelo.addColumn("Parroquia");
            modelo.addColumn("Instalación"
                    + " Deportiva");
            modelo.addColumn("Fecha Inicio");
            modelo.addColumn("Fecha Fin");
            
            
           
               //agregar filas
               while(rstb.next()){//leer registro
                 
                   String fila[]= new String[col];//arreglo
                   for(int j=0;j<col;j++){
                       fila[j]=rstb.getString(j+1);
                   } 
                   modelo.addRow(fila);
               }
               tabla.setModel(modelo);
               rstb.close();
               cn.close();
               
            
        //si no pasa al catch
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    
    }
    
}
