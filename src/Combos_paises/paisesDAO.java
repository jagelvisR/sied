/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import Conexion.conexion;

/**
 *
 * @author jagelvis1
 */
public class paisesDAO {
    
    public void mostrar_paises(JComboBox box){
        
        DefaultComboBoxModel value;
        conexion conec = new conexion();
        Statement st = null;
        Connection con = null;
        ResultSet rs = null;
        try{
            con = conec.conectar();
            st = con.createStatement();
            rs = st.executeQuery("SELECT * FROM pais");
            value = new DefaultComboBoxModel();
            box.setModel(value);
            while(rs.next()){
                value.addElement(new paisesVO(rs.getInt(1),rs.getString(2)));
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                st.close();
                rs.close();
                conec.desconectar();
            }catch(Exception ex){
            }
        }
        
    }
    
}
