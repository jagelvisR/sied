/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;



/**
 *
 * @author jagelvis1
 */
public class parroquiaVO {
    
    private int id_parroquia;
    private int id_municipio;
    private String parroquia;
    
    public parroquiaVO(){}
    
    public parroquiaVO(int id, String nombre){
        this.id_parroquia = id;
        this.parroquia = nombre;
    }

    /**
     * @return the id_parroquia
     */
    public int getId_parroquia() {
        return id_parroquia;
    }

    /**
     * @param id_parroquia the id_parroquia to set
     */
    public void setId_parroquia(int id_parroquia) {
        this.id_parroquia = id_parroquia;
    }

    /**
     * @return the id_municipio
     */
    public int getId_municipio() {
        return id_municipio;
    }

    /**
     * @param id_municipio the id_municipio to set
     */
    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    /**
     * @return the parroquia
     */
    public String getParroquia() {
        return parroquia;
    }

    /**
     * @param parroquia the parroquia to set
     */
    public void setParroquia(String parroquia) {
        this.parroquia = parroquia;
    }
    
    public String toString(){
        return this.parroquia;
    }
}
