/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import Conexion.conexion;

/**
 *
 * @author jagelvis1
 */
public class estadoDAO {
    
    public void mostrar_estados(JComboBox box, int id){
        
        DefaultComboBoxModel value;
        conexion conec = new conexion();
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        try{
            con = conec.conectar();
            ps = con.prepareStatement("select * FROM estado where id_pais = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            value = new DefaultComboBoxModel();
            box.setModel(value);
            while(rs.next()){
                value.addElement(new estadoVO(rs.getInt(1),rs.getString(3)));
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                rs.close();
                con.close();
            }catch(Exception ex){
            }
        }
        
    }
    
}
