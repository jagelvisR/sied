/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;

/**
 *
 * @author jagelvis1
 */
public class estadoVO {
    
    private int id_est;
    private int id_pais;
    private String nombre_est;
    
    public estadoVO(){}
    
    public estadoVO(int  id, String nombre){
        this.id_est = id;
        this.nombre_est  = nombre;
    }

    /**
     * @return the id_est
     */
    public int getId_est() {
        return id_est;
    }

    /**
     * @param id_est the id_est to set
     */
    public void setId_est(int id_est) {
        this.id_est = id_est;
    }

    /**
     * @return the id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais the id_pais to set
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return the nombre_est
     */
    public String getNombre_est() {
        return nombre_est;
    }

    /**
     * @param nombre_est the nombre_est to set
     */
    public void setNombre_est(String nombre_est) {
        this.nombre_est = nombre_est;
    }
    
    public String toString(){
        return this.nombre_est;
    }
    
}
