/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;

/**
 *
 * @author jagelvis1
 */
public class municipioVO {
    
    private int id_municipio;
    private int id_est;
    private String municipio;
    
    public municipioVO(){}
    
    public municipioVO(int id, String nombre){
        this.id_municipio = id;
        this.municipio = nombre;
    }
    

    /**
     * @return the id_municipio
     */
    public int getId_municipio() {
        return id_municipio;
    }

    /**
     * @param id_municipio the id_municipio to set
     */
    public void setId_municipio(int id_municipio) {
        this.id_municipio = id_municipio;
    }

    /**
     * @return the id_est
     */
    public int getId_est() {
        return id_est;
    }

    /**
     * @param id_est the id_est to set
     */
    public void setId_est(int id_est) {
        this.id_est = id_est;
    }

    /**
     * @return the municipio
     */
    public String getMunicipio() {
        return municipio;
    }

    /**
     * @param municipio the municipio to set
     */
    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
    
    public String toString(){
        return this.municipio;
    }
}
