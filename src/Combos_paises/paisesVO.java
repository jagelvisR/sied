/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Combos_paises;

/**
 *
 * @author jagelvis1
 */
public class paisesVO {
    
    private int id_pais;
    private String nombre_pais;
    
    public paisesVO(){}
    
    public paisesVO(int id,String nombre){
        this.id_pais = id;
        this.nombre_pais = nombre;
        
    }


    /**
     * @return the id_pais
     */
    public int getId_pais() {
        return id_pais;
    }

    /**
     * @param id_pais the id_pais to set
     */
    public void setId_pais(int id_pais) {
        this.id_pais = id_pais;
    }

    /**
     * @return the nombre_pais
     */
    public String getNombre_pais() {
        return nombre_pais;
    }

    /**
     * @param nombre_pais the nombre_pais to set
     */
    public void setNombre_pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
    }
    
    public String toString(){
        return  this.nombre_pais;
    }
    
}
