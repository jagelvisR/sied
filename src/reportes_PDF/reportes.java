/*
encargado de generar los reportes
 */
package reportes_PDF;


import Conexion.conexion;
import com.toedter.calendar.JDateChooser;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author jagelvis1
 */
public class reportes {
    
    
    //metodo para conectar
     Connection cn;//variable conexion
    
      //metodo para crear pdf de nomina de atletas
    public void reporte_nomina_atletas(JTable tabla, JComboBox Dep, JComboBox Sexo){
      
        try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
          
          String master =  "SIED\\src\\Login\\nomina_atletas.jasper";  
          JasperReport reporte;
            reporte = (JasperReport) JRLoader.loadObject(master);
          
          Map parametros = new HashMap();
          parametros.put("Deporte",Dep.getSelectedItem().toString());
          parametros.put("Sexo",Sexo.getSelectedItem().toString());
          
          JasperPrint j= JasperFillManager.fillReport(reporte, parametros,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Nomina Definitiva de Atletlas por Deporte y Sexo");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    }
    
    //metodo para crear pdf de Calendario General de Competencias
    public void reporte_calendario_eventos(JTable tabla){
      try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject("C:\\Users\\jagelvis1\\Documents\\PROYECTO EVENTOS DEPORTIVOS\\SIED\\src\\Login\\calendario_eventos.jasper");
        
         
          JasperPrint j= JasperFillManager.fillReport(reporte, null,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Calendario General de Competencias");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    } 
    
    public void reporte_calendario_eventos_por_deporte(JTable tabla,JComboBox Dep,JComboBox fase,String IND,String Min,JDateChooser inicio,JDateChooser fin) throws IOException{
      try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
         String fecha_inicio = null;//asignar variable temporal
            java.util.Date fecha=(java.util.Date) inicio.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_inicio = sdf.format(fecha);
            
            String fecha_fin = null;//asignar variable temporal
            java.util.Date fecha2=(java.util.Date) fin.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_fin =sdf2.format(fecha2);
               
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject(this.getClass().getResource("/reportes_PDF/calendario_eventos_por_deporte.jasper"));
          
          Map parametros = new HashMap();
          parametros.put("Deporte",Dep.getSelectedItem().toString());
          parametros.put("Fase",fase.getSelectedItem().toString());
          parametros.put("IND",this.getClass().getResource(IND).openStream());
          parametros.put("Min_dep",this.getClass().getResource(Min).openStream());
          parametros.put("Inicio",fecha_inicio);
          parametros.put("Fin",fecha_fin);
          JasperPrint j= JasperFillManager.fillReport(reporte, parametros,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Calendario Integrado de Eventos Deportivos");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
//    
    } 
    
    public void reporte_calendario_eventos_por_entidad(JTable tabla,JComboBox pais,JComboBox Ent,JComboBox fase,String IND,String Min,JDateChooser inicio,JDateChooser fin) throws IOException{
      try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
             String fecha_inicio = null;//asignar variable temporal
            java.util.Date fecha=(java.util.Date) inicio.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_inicio = sdf.format(fecha);
            
            String fecha_fin = null;//asignar variable temporal
            java.util.Date fecha2=(java.util.Date) fin.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf2=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_fin =sdf2.format(fecha2);
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject(this.getClass().getResource("/reportes/calendario_eventos_por_entidad.jasper"));
        
           Map parametros = new HashMap();
          parametros.put("Pais",pais.getSelectedItem().toString()); 
          parametros.put("Entidad",Ent.getSelectedItem().toString());
          parametros.put("Fase",fase.getSelectedItem().toString());
          parametros.put("IND",this.getClass().getResource(IND).openStream());
          parametros.put("Min_dep",this.getClass().getResource(Min).openStream());
          parametros.put("Inicio",fecha_inicio);
          parametros.put("Fin",fecha_fin);
          
          JasperPrint j= JasperFillManager.fillReport(reporte, parametros,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Calendario General de Competencias por Entidad");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    } 
    
    //metodo para crear pdf de reporte general por pais y entidad 
    public void reporte_cuadro_general_pais_entidad(JTable tabla,JComboBox Deporte,JComboBox Pais,JComboBox Entidad,String IND,String Min) throws IOException{
      try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject(this.getClass().getResource("/reportes/cuadro_general_pais_entidad.jasper"));
          
          Map parametros = new HashMap();
          parametros.put("Deporte",Deporte.getSelectedItem().toString());
          parametros.put("Pais",Pais.getSelectedItem().toString()); 
          parametros.put("Entidad",Entidad.getSelectedItem().toString());
          parametros.put("IND",this.getClass().getResource(IND).openStream());
          parametros.put("Min_dep",this.getClass().getResource(Min).openStream());
        
          
          JasperPrint j= JasperFillManager.fillReport(reporte, parametros,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Cuadro general de medallas por pais y entidad");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    }
    
      //metodo para crear pdf de resultados por deporte
    public void reporte_resultado_deporte_atleta(JTable tabla, JComboBox Dep, JDateChooser fecha_med,String IND,String Min) throws IOException{
      
        try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
            String fecha_md = null;//asignar variable temporal
            java.util.Date fecha=(java.util.Date) fecha_med.getDate();//tomar la fecha
            //del jdatechooser
            //establecer el formato
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            //asignar la fecha con el nuevo formato a la variable
            fecha_md = sdf.format(fecha);
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject(this.getClass().getResource("/reportes/resultados_deporte.jasper"));
          
          Map parametros = new HashMap();
          parametros.put("Deporte",Dep.getSelectedItem().toString());
          parametros.put("Fecha_med",fecha_md);
          parametros.put("IND",this.getClass().getResource(IND).openStream());
          parametros.put("Min_dep",this.getClass().getResource(Min).openStream());
          
          JasperPrint j= JasperFillManager.fillReport(reporte, parametros,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("Resultados por Deporte");
         
         //hacer q sea visible
         jv.setVisible(true);
         
         

        }catch (JRException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    }
    
    
    
    
    
    //metodo para crear pdf 
    public void reporte2(JTable tabla2){
      try{
           //establecer con conexion mediante el objeto
            conexion con = new conexion();
            cn=con.conectar();
            
          JasperReport reporte=(JasperReport) JRLoader.loadObject("reporte2.jasper");
         //instaciar el objeto jasper y q cargue la plantilla reporte
          
          JasperPrint j= JasperFillManager.fillReport(reporte, null,cn);
         //enviar a jasper la plantilla del reporte, parametro (null por que no hay) y conexion
         
         //para vizualizar
         JasperViewer jv=new JasperViewer(j,false);
         //para seleccionar el nombre
         jv.setTitle("atletas multimedallistas");
         
         //hacer q sea visible
         jv.setVisible(true);
         
       
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "error durante el proceso");
        }  
    
    } 

    
}