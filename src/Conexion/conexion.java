/*
 Clase encargada de establecer la conexion con la base de datos
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author magowind
 */
public class conexion {
   
    
    //variables para establecer la conexion
    static String driver = "com.mysql.jdbc.Driver";//driver de mysql --> libreria
    static String ruta = "jdbc:mysql://localhost:3306/sied";//ubicacion de la base de datos
    static String usuario = "root";//nombre de usuario
    static String clave = "";//clave de usuario
    
    Connection conectar=null;//estado de la conexion
    
    public Connection conectar(){//metodo 1
        
        try{
            Class.forName(driver);
            conectar = DriverManager.getConnection(ruta,usuario,clave);
            
        
        }   catch (SQLException ex) {
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   catch (ClassNotFoundException ex) { 
            Logger.getLogger(conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return conectar;
     
    
}
     public void desconectar(){
         conectar = null;
     }
    
     public static void main (String[] args) {
         conexion c = new conexion();
         c.conectar();
     }
    
    
    

    public ResultSet executeQuery(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
